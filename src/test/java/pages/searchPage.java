package pages;

import org.openqa.selenium.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class searchPage {
    WebDriver driver;

    public searchPage(WebDriver driver) {
        this.driver = driver;
    }

    By minimumPriceFilter = By.cssSelector("input[placeholder='Harga Minimum']");

    By minimumPriceFilterLabel = By.xpath("//button[normalize-space()='Harga Minimum']");

    By maximumPriceFilter = By.cssSelector("input[placeholder='Harga Maksimum']");

    By maximumPriceFilterLabel = By.xpath("//button[normalize-space()='Harga Maksimum']");

    By officialStoreFilter = By.cssSelector("body > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)");

    By officialStoreFilterLabel = By.xpath("//button[normalize-space()='Official Store']");
    
    By sortDropdown = By.cssSelector(".css-1ro2jof.e83okfj0");

    By lowestPriceSort = By.cssSelector("button[data-item-text='Harga Terendah']");

    By itemNameList = By.xpath("//*[@data-testid='spnSRPProdName']");

    By pagination = By. cssSelector("button[aria-label='Laman berikutnya']");

    public void setMinimumPriceFilter(String minPrice) throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, 500);");
        Thread.sleep(2000);

        driver.findElement(minimumPriceFilter).sendKeys(minPrice);
        driver.findElement(minimumPriceFilter).sendKeys(Keys.RETURN);
        Thread.sleep(3000);
    }

    public void verifyMinimumLabelDisplayed() {
        driver.findElement(minimumPriceFilterLabel).isDisplayed();
    }

    public void setMaximumPriceFilter(String maxPrice) throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, 500);");
        Thread.sleep(2000);

        driver.findElement(maximumPriceFilter).sendKeys(maxPrice);
        driver.findElement(maximumPriceFilter).sendKeys(Keys.RETURN);
        Thread.sleep(3000);
    }

    public void verifyMaximumLabelDisplayed() {
        driver.findElement(maximumPriceFilterLabel).isDisplayed();
    }

    public void setOfficialStoreFilter() throws InterruptedException {
        driver.findElement(officialStoreFilter).click();
        Thread.sleep(3000);
    }

    public void verifyOfficialStoreLabel() {
        driver.findElement(officialStoreFilterLabel).isDisplayed();
    }

    public void setSortToLowestPrice() throws InterruptedException {
        driver.findElement(sortDropdown).click();
        Thread.sleep(2000);
        driver.findElement(lowestPriceSort).click();
        Thread.sleep(3000);
    }

    public void getItemNameList() throws InterruptedException, IOException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
        Thread.sleep(2000);

        List<WebElement> itemElements = driver.findElements(itemNameList);
        List<String> itemNames = new ArrayList<>();

        for (WebElement itemElement : itemElements) {
            String itemName = itemElement.getText().trim();
            itemNames.add(itemName);
        }

        String timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String filePath = "target/item_names_list_" + timestamp + ".txt";
        FileWriter fileWriter = new FileWriter(filePath);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        for (String itemName : itemNames) {
            bufferedWriter.write(itemName);
            bufferedWriter.newLine();
        }

        bufferedWriter.close();
        fileWriter.close();
        Thread.sleep(2000);
    }

    public void goToNextPage() throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
        Thread.sleep(2000);
        js.executeScript("window.scrollTo(0, -100);");
        Thread.sleep(2000);

        driver.findElement(pagination).click();
        Thread.sleep(3000);
    }
}
