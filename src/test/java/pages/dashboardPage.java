package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class dashboardPage {
    WebDriver driver;
    public dashboardPage(WebDriver driver) {
        this.driver = driver;
    }

    By searchBoxField = By.cssSelector("input[placeholder='Cari di Tokopedia']");

    By searchResultContains = By.cssSelector("div[class='css-x8pgyn'] strong");

    public void searchItemOnSearchBox(String search) throws InterruptedException {
        driver.findElement(searchBoxField).sendKeys(search);
        driver.findElement(searchBoxField).sendKeys(Keys.RETURN);
        Thread.sleep(10000);
    }

    public void verifySearchResultContains(String contains) {
        String actualString = driver.findElement(searchResultContains).getText();
        assertTrue(actualString.contains(contains));
    }
}
