package stepDefinitions;

import io.cucumber.java.en.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.dashboardPage;
import pages.searchPage;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class searchSteps {
    WebDriver driver = null;
    dashboardPage dashboard;
    searchPage search;

    @Given("user navigate to Tokopedia dashboard")
    public void user_navigate_to_tokopedia_dashboard() {
        String projectPath = System.getProperty("user.dir");
        System.setProperty("webdriver.chrome.driver", projectPath+"/src/test/resources/drivers/chromedriver.exe");

        driver = new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.navigate().to("https://www.tokopedia.com");
    }

    @When("user search {string} on searchbox")
    public void user_search_on_searchbox(String search) throws InterruptedException {
        dashboard = new dashboardPage(driver);
        dashboard.searchItemOnSearchBox(search);
    }

    @And("user verify search item contains {string}")
    public void user_verify_search_item_contains(String contains) throws InterruptedException {
        dashboard.verifySearchResultContains(contains);
    }

    @And("user set minimum price filter to {string}")
    public void user_set_minimum_price_filter_to(String minPrice) throws InterruptedException {
        search = new searchPage(driver);
        search.setMinimumPriceFilter(minPrice);
    }

    @And("user verify Harga Minimum label is displayed")
    public void user_verify_harga_minimum_label_is_displayed() {
        search.verifyMinimumLabelDisplayed();
    }

    @And("user set maximum price filter to {string}")
    public void user_set_maximum_price_filter_to(String maxPrice) throws InterruptedException {
        search.setMaximumPriceFilter(maxPrice);
    }

    @And("user verify Harga Maksimum label is displayed")
    public void user_verify_harga_maksimum_label_is_displayed() {
        search.verifyMaximumLabelDisplayed();
    }

    @And("user check Official Store checkbox")
    public void user_check_official_store_checkbox() throws InterruptedException {
        search.setOfficialStoreFilter();
    }

    @And("user verify Official Store label is displayed")
    public void user_verify_official_store_label_is_displayed() {
        search.verifyOfficialStoreLabel();
    }

    @And("user sort item to Lowest Price")
    public void user_sort_item_to_lowest_price() throws InterruptedException {
        search.setSortToLowestPrice();
    }

    @Then("user get item name from list")
    public void user_get_item_name_from_list() throws InterruptedException, IOException {
        search.getItemNameList();
    }

    @And("user go to next page")
    public void user_go_to_next_page() throws InterruptedException {
        search.goToNextPage();
    }

    @And("user close browser")
    public void user_close_browser() {
        driver.quit();
    }

}
