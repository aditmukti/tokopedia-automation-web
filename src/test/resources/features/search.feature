@search
Feature: feature to search item from dashboard searchbox

  Scenario: Search item and get item list
    Given user navigate to Tokopedia dashboard
    When user search "iphone 14 pro" on searchbox
    And user verify search item contains "iphone 14 pro"
    And user set minimum price filter to "100000"
    And user verify Harga Minimum label is displayed
    And user set maximum price filter to "30000000"
    And user verify Harga Maksimum label is displayed
    And user check Official Store checkbox
    And user verify Official Store label is displayed
    And user sort item to Lowest Price
    Then user get item name from list
    And user go to next page
    Then user get item name from list
    And user go to next page
    Then user get item name from list
    And user close browser
